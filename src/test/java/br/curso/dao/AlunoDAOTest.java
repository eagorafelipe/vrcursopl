package br.curso.dao;

import br.curso.DAO.AlunoDAO;
import br.curso.VO.Aluno.AlunoVO;
import org.junit.Test;

/**
 *
 * @author felipe.bruno
 */
public class AlunoDAOTest {

    public AlunoDAOTest() {
    }

    @Test
    public void salvarAluno() throws Exception {
        AlunoVO oAluno = new AlunoVO();
        oAluno.setNome("ADAMASTOR");
        oAluno.setRg("233904591");
        oAluno.setCpf("21717044034");
        oAluno.setMatricula("47747FLAJICTC");

        new AlunoDAO().salvar(oAluno);
    }

}
