/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.curso.dao;

import br.curso.DAO.CursoDAO;
import br.curso.VO.Curso.CursoFiltroVO;
import br.curso.VO.Curso.CursoVO;
import br.curso.VO.Periodo;
import br.curso.VO.SituacaoCadastro;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author felipe.bruno
 */
public class CursoDAOTest {

    public CursoDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of consultar method, of class CursoDAO.
     */
    @Test
    public void testConsultar() throws Exception {
        System.out.println("consultar");

        CursoFiltroVO i_oFiltro = new CursoFiltroVO();
        i_oFiltro.descricao = "CURSO DE MONSTROS";
        i_oFiltro.id = 2;
        i_oFiltro.situacaoCadastro = SituacaoCadastro.ATIVO.getDescricao();
        i_oFiltro.periodo = Periodo.MATUTINO.getDescricao();

        CursoDAO instance = new CursoDAO();
        ArrayList<CursoVO> expResult = new ArrayList<>();
        ArrayList<CursoVO> result = instance.consultar(i_oFiltro);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of salvar method, of class CursoDAO.
     */
    @Test
    public void testSalvar() throws Exception {
        System.out.println("salvar");

        CursoVO oCurso = new CursoVO();
        oCurso.setDescricao("CURSO SOBRE CURSO");
        oCurso.setCargaHoraria(12);
        oCurso.setDuracao("12:32");
        oCurso.setIdSituacaoCadastro(SituacaoCadastro.ATIVO.getId());
        oCurso.setQtdAlunos(32);
        oCurso.setPeriodo(Periodo.INTEGRAL.getDescricao());

        CursoDAO instance = new CursoDAO();
        instance.salvar(oCurso);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of carregar method, of class CursoDAO.
     */
    @Test
    public void testCarregar() throws Exception {
        System.out.println("carregar");
        int i_id = 2;
        CursoDAO instance = new CursoDAO();
        CursoVO expResult = new CursoVO();
        CursoVO result = instance.carregar(i_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of excluir method, of class CursoDAO.
     */
    @Test
    public void testExcluir() throws Exception {
        System.out.println("excluir");
        int i_id = 1;
        CursoDAO instance = new CursoDAO();
        instance.excluir(i_id);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
