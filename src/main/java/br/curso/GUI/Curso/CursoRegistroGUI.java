package br.curso.GUI.Curso;

import br.curso.DAO.CursoDAO;
import br.curso.GUI.Aluno.AlunoCadastroGUI;
import br.curso.GUI.Disciplina.DisciplinaCadastroGUI;
import br.curso.GUI.InicioGUI;
import br.curso.VO.Aluno.AlunoVO;
import br.curso.VO.Curso.CursoVO;
import br.curso.VO.Disciplina.DisciplinaVO;
import br.curso.VO.Periodo;
import br.curso.VO.SituacaoCadastro;
import br.curso.classes.JTextFieldLimit;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author felipe.bruno
 */
public class CursoRegistroGUI extends javax.swing.JInternalFrame {

    CursoVO oCurso = new CursoVO();

    ArrayList<DisciplinaVO> vDisciplina = new ArrayList();
    ArrayList<AlunoVO> vAluno = new ArrayList();

    DefaultTableModel modelDisciplina = null;
    DefaultTableModel modelAluno = null;

    public JDesktopPane desktop = null;

    public CursoRegistroGUI(JDesktopPane i_desktop) throws Exception {
        initComponents();

        desktop = i_desktop;

        cboSituacao.addItem(SituacaoCadastro.ATIVO.getDescricao());
        cboSituacao.addItem(SituacaoCadastro.EXCLUIDO.getDescricao());

        cboPeriodo.addItem(Periodo.MATUTINO.getDescricao());
        cboPeriodo.addItem(Periodo.VESPERTINO.getDescricao());
        cboPeriodo.addItem(Periodo.NOTURNO.getDescricao());
        cboPeriodo.addItem(Periodo.INTEGRAL.getDescricao());

        cboPeriodo.setSelectedIndex(Periodo.MATUTINO.getId());

        txtDescricao.setDocument(new JTextFieldLimit(40));
        txtQtdAlunos.setDocument(new JTextFieldLimit(3));
        txtCargaHoraria.setDocument(new JTextFieldLimit(3));

        configurarColunaAluno();
        configurarColunaDisciplina();

    }

    public void incluir() throws Exception {
        oCurso = new CursoVO();

        txtCodigo.setText("");
        txtCodigo.setEnabled(false);
        txtDescricao.setText("");
        txtQtdAlunos.setText("");
        txtDuracao.setText("");
        txtCargaHoraria.setText("");
        cboPeriodo.setSelectedIndex(-1);
        cboSituacao.setSelectedIndex(SituacaoCadastro.ATIVO.getId());
        cboSituacao.setEnabled(false);
        vDisciplina.clear();
        vAluno.clear();

        exibirConsultaDisciplina();
        exibirConsultaAluno();
    }

    public void salvar() throws Exception {

        if (txtDescricao.getText().isEmpty()) {
            exibirAlerta("Campo nome vazio!");
            txtDescricao.requestFocus();

        } else if (txtDuracao.getText().isEmpty()) {
            exibirAlerta("Campo Dura��o vazio!");
            txtDuracao.requestFocus();

        } else if (txtQtdAlunos.getText().isEmpty()) {
            exibirAlerta("Informe a quantidade de alunos!");
            txtQtdAlunos.requestFocus();

        } else if (txtCargaHoraria.getText().isEmpty()) {
            exibirAlerta("Informe a carga hor�ria!");
            txtCargaHoraria.requestFocus();

        } else if (Integer.parseInt(txtCargaHoraria.getText()) < 20 || Integer.parseInt(txtCargaHoraria.getText()) > 40) {
            exibirAlerta("Os valores da carga hor�ria devem ser entre 20 e 40!");
            txtCargaHoraria.requestFocus();

        } else {

            try {
                oCurso = new CursoVO();

                oCurso.setCodigo(txtCodigo.getText().isEmpty() ? 0 : Integer.parseInt(txtCodigo.getText()));
                oCurso.setDescricao(txtDescricao.getText().toUpperCase());
                oCurso.setDuracao(txtDuracao.getText());
                oCurso.setCargaHoraria(Integer.parseInt(txtCargaHoraria.getText()));
                oCurso.setQtdAlunos(Integer.parseInt(txtQtdAlunos.getText()));
                oCurso.setIdSituacaoCadastro(cboSituacao.getSelectedIndex());
                oCurso.setPeriodo(cboPeriodo.getSelectedItem().toString());
                oCurso.setvDisciplina(vDisciplina);
                oCurso.setvAluno(vAluno);

                new CursoDAO().salvar(oCurso);

                JOptionPane.showMessageDialog(this, "Curso salvo com sucesso!", title, JOptionPane.PLAIN_MESSAGE);
                incluir();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void carregar(int i_id) throws Exception {
        oCurso = new CursoDAO().carregar(i_id);

        if (oCurso.getCodigo() != 0) {
            txtCodigo.setText(String.format("%04d", oCurso.getCodigo()));
            txtDescricao.setText(oCurso.getDescricao());
            txtQtdAlunos.setText(String.valueOf(oCurso.getQtdAlunos()));
            txtCargaHoraria.setText(String.valueOf(oCurso.getCargaHoraria()));
            txtDuracao.setText(oCurso.getDuracao());
            cboSituacao.setSelectedIndex(oCurso.getIdSituacaoCadastro());
            cboSituacao.setEnabled(true);
            vDisciplina = oCurso.getvDisciplina();
            vAluno = oCurso.getvAluno();
        }

        exibirConsultaDisciplina();
        exibirConsultaAluno();
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }

    private void exibirAlerta(String i_mensagem) {
        JOptionPane.showMessageDialog(this, i_mensagem, title, JOptionPane.WARNING_MESSAGE);
    }

    public void configurarColunaDisciplina() throws Exception {
        modelDisciplina = (DefaultTableModel) tblDisciplina.getModel();
        modelDisciplina.setRowCount(0);
    }

    public void exibirConsultaDisciplina() throws Exception {
        configurarColunaDisciplina();

        for (DisciplinaVO oDisciplina : vDisciplina) {
            Object o[] = {
                oDisciplina.getDescricao(),
                oDisciplina.getProfessor(),
                oDisciplina.getDiaSemana()
            };

            modelDisciplina.addRow(o);
        }

        tblDisciplina.setModel(modelDisciplina);
    }

    public void configurarColunaAluno() throws Exception {
        modelAluno = (DefaultTableModel) tblAluno.getModel();
        modelAluno.setRowCount(0);
    }

    public void exibirConsultaAluno() throws Exception {
        configurarColunaAluno();

        for (AlunoVO oAluno : vAluno) {
            Object o[] = {
                String.format("%04d", oAluno.getCodigo()),
                oAluno.getNome()
            };

            modelAluno.addRow(o);
        }

        tblAluno.setModel(modelAluno);
    }

    public void carregarDisciplina() throws Exception {
        if (InicioGUI.formDisciplinaCadastro == null || InicioGUI.formDisciplinaCadastro.isClosed()) {
            InicioGUI.formDisciplinaCadastro = new DisciplinaCadastroGUI(desktop, this);
            desktop.add(InicioGUI.formDisciplinaCadastro);
            InicioGUI.formDisciplinaCadastro.setPosicao();
        }

        InicioGUI.formDisciplinaCadastro.setVisible(true);
        InicioGUI.formDisciplinaCadastro.toFront();
    }

    public void addDisciplina(DisciplinaVO i_oDisciplina) throws Exception {
        boolean achou = false;

        achou = getDiaSemanaDisciplina(i_oDisciplina);
        achou = getCargaHorariaCurso(i_oDisciplina);

        if (!achou) {
            vDisciplina.add(i_oDisciplina);
        }

        exibirConsultaDisciplina();
    }

    public boolean getDiaSemanaDisciplina(DisciplinaVO i_oDisciplina) throws Exception {
        boolean achou = false;
        if (oCurso.getCodigo() > 0) {
            achou = new CursoDAO().getDiaSemanaDisciplina(i_oDisciplina.getDiaSemana());
            achou = (new CursoDAO().getCargaHorariaCurso(oCurso.getCodigo()) + i_oDisciplina.getCargaHoraria()) > oCurso.getCargaHoraria();

        } else {
            for (DisciplinaVO oDisciplina : vDisciplina) {
                if (oDisciplina.getDiaSemana().equals(i_oDisciplina.getDiaSemana())) {
                    achou = true;
                    break;
                }
            }
        }

        if (achou) {
            JOptionPane.showMessageDialog(this, "O curso j� possu� uma disciplina no dia " + i_oDisciplina.getDiaSemana(), title, JOptionPane.WARNING_MESSAGE);
        }

        return achou;
    }

    public boolean getCargaHorariaCurso(DisciplinaVO i_oDisciplina) throws Exception {
        boolean achou = false;
        int cargaHoraria = 0;

        if (oCurso.getCodigo() > 0) {
            achou = (new CursoDAO().getCargaHorariaCurso(oCurso.getCodigo()) + i_oDisciplina.getCargaHoraria()) > oCurso.getCargaHoraria();

        } else {
            for (DisciplinaVO oDisciplina : vDisciplina) {
                if ((cargaHoraria + oDisciplina.getCargaHoraria()) > Integer.parseInt(txtCargaHoraria.getText())) {
                    achou = true;
                    break;

                } else {
                    cargaHoraria += oDisciplina.getCargaHoraria();
                }
            }
        }

        if (achou) {
            JOptionPane.showMessageDialog(this, "A inclus�o da discinplina ultrapassa a carga hor�ria m�xima do curso", title, JOptionPane.WARNING_MESSAGE);
        }

        return achou;
    }

    public void removerDisciplina() throws Exception {
        if (tblDisciplina.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum item selecionado!", title, JOptionPane.WARNING_MESSAGE);

        } else {
            vDisciplina.remove(tblDisciplina.getSelectedRow());

            exibirConsultaDisciplina();
        }

    }

    public void carregarAluno() throws Exception {
        if (InicioGUI.formAlunoCadastro == null || InicioGUI.formAlunoCadastro.isClosed()) {
            InicioGUI.formAlunoCadastro = new AlunoCadastroGUI(desktop, this);
            desktop.add(InicioGUI.formAlunoCadastro);
            InicioGUI.formAlunoCadastro.setPosicao();
        }

        InicioGUI.formAlunoCadastro.setVisible(true);
        InicioGUI.formAlunoCadastro.toFront();
    }

    public void addAluno(AlunoVO i_oAluno) throws Exception {
        vAluno.add(i_oAluno);

        exibirConsultaAluno();
    }

    public void removerAluno() throws Exception {
        if (tblAluno.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum item selecionado!", title, JOptionPane.WARNING_MESSAGE);
        } else {
            vAluno.remove(tblAluno.getSelectedRow());

            exibirConsultaAluno();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtDescricao = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cboSituacao = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtDuracao = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        cboPeriodo = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtQtdAlunos = new javax.swing.JTextField();
        txtCargaHoraria = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        btnAdicionar = new javax.swing.JButton();
        btnRemover = new javax.swing.JButton();
        abas = new javax.swing.JTabbedPane();
        abaDisciplina = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblDisciplina = new javax.swing.JTable();
        abaAluno = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAluno = new javax.swing.JTable();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Registro Curso");

        jToolBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jToolBar1.setRollover(true);

        btnNovo.setText("Novo");
        btnNovo.setFocusable(false);
        btnNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnNovo);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("C�digo");

        txtCodigo.setEnabled(false);

        txtDescricao.setColumns(80);

        jLabel2.setText("Descri��o");

        jLabel3.setText("Dura��o");

        jLabel6.setText("Situa��o");

        jLabel4.setText("Qtd. Alunos");

        txtDuracao.setColumns(4);
        try {
            txtDuracao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel5.setText("Per�odo");

        jLabel7.setText("Carga Hor�ria");

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnAdicionar.setText("Adicionar");
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        btnRemover.setText("Remover");
        btnRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRemover)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAdicionar)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdicionar)
                    .addComponent(btnRemover)))
        );

        tblDisciplina.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Disciplina", "Professor", "Dia Semana"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDisciplina.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(tblDisciplina);

        javax.swing.GroupLayout abaDisciplinaLayout = new javax.swing.GroupLayout(abaDisciplina);
        abaDisciplina.setLayout(abaDisciplinaLayout);
        abaDisciplinaLayout.setHorizontalGroup(
            abaDisciplinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(abaDisciplinaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );
        abaDisciplinaLayout.setVerticalGroup(
            abaDisciplinaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(abaDisciplinaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        abas.addTab("Disciplina", abaDisciplina);

        tblAluno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "C�digo", "Aluno"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblAluno.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tblAluno);

        javax.swing.GroupLayout abaAlunoLayout = new javax.swing.GroupLayout(abaAluno);
        abaAluno.setLayout(abaAlunoLayout);
        abaAlunoLayout.setHorizontalGroup(
            abaAlunoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(abaAlunoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );
        abaAlunoLayout.setVerticalGroup(
            abaAlunoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(abaAlunoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        abas.addTab("Aluno", abaAluno);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(abas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtCodigo)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel3)
                                    .addComponent(txtDuracao))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtQtdAlunos))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtCargaHoraria, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5)
                                            .addComponent(cboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addComponent(jLabel6)
                            .addComponent(cboSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtDuracao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtQtdAlunos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(25, 25, 25))
                            .addComponent(txtCargaHoraria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(26, 26, 26)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(26, 26, 26))
                            .addComponent(cboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)))
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(abas, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            salvar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        try {
            dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        try {
            incluir();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        try {
            if (abas.getSelectedComponent() == abaDisciplina) {
                carregarDisciplina();

            } else if (abas.getSelectedComponent() == abaAluno) {
                carregarAluno();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverActionPerformed
        try {
            if (abas.getSelectedComponent() == abaDisciplina) {
                removerDisciplina();

            } else if (abas.getSelectedComponent() == abaAluno) {
                removerAluno();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnRemoverActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel abaAluno;
    private javax.swing.JPanel abaDisciplina;
    private javax.swing.JTabbedPane abas;
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnRemover;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cboPeriodo;
    private javax.swing.JComboBox<String> cboSituacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tblAluno;
    private javax.swing.JTable tblDisciplina;
    private javax.swing.JTextField txtCargaHoraria;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtDescricao;
    private javax.swing.JFormattedTextField txtDuracao;
    private javax.swing.JTextField txtQtdAlunos;
    // End of variables declaration//GEN-END:variables
}
