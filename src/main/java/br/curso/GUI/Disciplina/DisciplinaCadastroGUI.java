package br.curso.GUI.Disciplina;

import br.curso.DAO.DisciplinaDAO;
import br.curso.GUI.Curso.CursoRegistroGUI;
import br.curso.GUI.InicioGUI;
import br.curso.GUI.Professor.ProfessorCadastroGUI;
import br.curso.VO.DiaSemana;
import br.curso.VO.Disciplina.DisciplinaFiltroVO;
import br.curso.VO.Disciplina.DisciplinaVO;
import br.curso.VO.SituacaoCadastro;
import br.curso.classes.JTextFieldLimit;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author felipe.bruno
 */
public class DisciplinaCadastroGUI extends javax.swing.JInternalFrame {

    ArrayList<DisciplinaVO> vDisciplina = new ArrayList<>();
    DefaultTableModel model = null;

    public JDesktopPane desktop = null;
    public JInternalFrame internalFrame = null;

    public DisciplinaCadastroGUI(JDesktopPane i_desktop, JInternalFrame i_internal) throws Exception {

        initComponents();

        desktop = i_desktop;
        internalFrame = i_internal;

        cboSituacao.addItem("TODOS");
        cboSituacao.addItem(SituacaoCadastro.ATIVO.getDescricao());
        cboSituacao.addItem(SituacaoCadastro.EXCLUIDO.getDescricao());
        cboSituacao.setSelectedItem("TODOS");

        cboDiaSemana.addItem("TODOS");
        cboDiaSemana.addItem(DiaSemana.DOMINGO.getDescricao());
        cboDiaSemana.addItem(DiaSemana.SEGUNDA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.TERCA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.QUARTA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.QUINTA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.SEXTA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.SABADO.getDescricao());
        cboDiaSemana.setSelectedItem("TODOS");

        txtNome.setDocument(new JTextFieldLimit(40));
        txtCodigo.setDocument(new JTextFieldLimit(3));
        txtCodProfessor.setDocument(new JTextFieldLimit(3));

    }

    public void consultar() throws Exception {
        vDisciplina = new ArrayList<>();

        DisciplinaFiltroVO oFiltro = new DisciplinaFiltroVO();
        oFiltro.id = txtCodigo.getText().isEmpty() ? -1 : Integer.parseInt(txtCodigo.getText());
        oFiltro.descricao = txtNome.getText();
        oFiltro.idProfessor = txtCodProfessor.getText().isEmpty() ? -1 : Integer.parseInt(txtCodProfessor.getText());
        oFiltro.situacaoCadastro = cboSituacao.getSelectedItem().toString();
        oFiltro.diaSemana = cboDiaSemana.getSelectedItem().toString();

        vDisciplina = new DisciplinaDAO().consultar(oFiltro);

        configurarColuna();
        exibirConsulta();

    }

    public void configurarColuna() throws Exception {
        model = (DefaultTableModel) tblConsulta.getModel();
        model.setRowCount(0);
    }

    public void exibirConsulta() throws Exception {
        for (DisciplinaVO oDisciplina : vDisciplina) {
            Object o[] = {
                String.format("%04d", oDisciplina.getCodigo()),
                oDisciplina.getDescricao(),
                oDisciplina.getEmenta(),
                oDisciplina.getLimiteVagas(),
                oDisciplina.getProfessor(),
                oDisciplina.getDiaSemana(),
                oDisciplina.getCargaHoraria(),
                oDisciplina.getSituacaoCadastro()
            };
            model.addRow(o);
        }

        tblConsulta.setModel(model);
    }

    public void incluir() throws Exception {
        DisciplinaRegistroGUI form = new DisciplinaRegistroGUI(desktop);
        desktop.add(form);
        form.setPosicao();
        form.incluir();
        form.setVisible(true);

    }

    public void carregarDisciplina() throws Exception {
        if (tblConsulta.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum item selecionado!", title, JOptionPane.WARNING_MESSAGE);

        } else {
            DisciplinaVO oDisciplina = vDisciplina.get(tblConsulta.getSelectedRow());

            if (internalFrame instanceof CursoRegistroGUI) {
                if (oDisciplina.getIdSituacaoCadastro() == SituacaoCadastro.EXCLUIDO.getId()) {
                    JOptionPane.showMessageDialog(this, "Disciplina exclu�do!", title, JOptionPane.WARNING_MESSAGE);

                } else {
                    ((CursoRegistroGUI) internalFrame).addDisciplina(oDisciplina);
                    InicioGUI.formDisciplinaCadastro.dispose();
                }

            } else {
                DisciplinaRegistroGUI form = new DisciplinaRegistroGUI(desktop);
                desktop.add(form);
                form.setPosicao();
                form.carregar(oDisciplina.getCodigo());
                form.setVisible(true);
            }

        }
    }

    public void excluir() throws Exception {
        if (tblConsulta.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum item selecionado!", title, JOptionPane.WARNING_MESSAGE);

        } else {
            DisciplinaVO oDisciplina = vDisciplina.get(tblConsulta.getSelectedRow());

            if (oDisciplina.getIdSituacaoCadastro() == SituacaoCadastro.EXCLUIDO.getId()) {
                JOptionPane.showMessageDialog(this, "Disciplina j� exclu�do!", title, JOptionPane.WARNING_MESSAGE);

            } else {
                JOptionPane.showConfirmDialog(this, "Deseja realmente excluir?", title, JOptionPane.OK_CANCEL_OPTION);

                new DisciplinaDAO().excluir(oDisciplina.getCodigo());

                JOptionPane.showMessageDialog(this, "Disciplina exclu�do com sucesso!", title, JOptionPane.PLAIN_MESSAGE);

            }
        }

    }

    public void setIdProfessor(int i_id) throws Exception {
        txtCodProfessor.setText(String.valueOf(i_id));
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnNovo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtNome = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnConsultar = new javax.swing.JButton();
        cboDiaSemana = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cboSituacao = new javax.swing.JComboBox<>();
        txtCodProfessor = new javax.swing.JTextField();
        btnConsultaProfessor = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblConsulta = new javax.swing.JTable();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Cadastro Disciplina");

        jToolBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jToolBar1.setRollover(true);

        btnNovo.setText("Novo");
        btnNovo.setFocusable(false);
        btnNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnNovo);

        btnEditar.setText("Editar");
        btnEditar.setFocusable(false);
        btnEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnEditar);

        btnExcluir.setText("Excluir");
        btnExcluir.setFocusable(false);
        btnExcluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExcluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnExcluir);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("C�digo");

        jLabel2.setText("Nome");

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        jLabel3.setText("Dia Semana");

        jLabel4.setText("Situa��o");

        btnConsultaProfessor.setIcon(new javax.swing.ImageIcon("C:\\git\\provaPL\\vrcursopl\\src\\img\\outline_search_lupa_find-512.png")); // NOI18N
        btnConsultaProfessor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnConsultaProfessorMouseClicked(evt);
            }
        });

        jLabel5.setText("Professor");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCodProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnConsultaProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(cboDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(cboSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnConsultar)
                .addContainerGap(199, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(btnConsultaProfessor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCodProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cboSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnConsultar)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(11, 11, 11))
        );

        tblConsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "C�digo", "Descri��o", "Ementa", "Limite Vagas", "Professor", "Dia Semana", "Carga Hor�ria", "Situacao Cadastro"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblConsulta.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblConsulta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblConsultaMouseClicked(evt);
            }
        });
        tblConsulta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblConsultaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblConsulta);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        try {
            consultar();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        try {
            incluir();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnNovoActionPerformed

    private void tblConsultaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblConsultaMouseClicked
        try {
            if (evt.getClickCount() == 2) {
                carregarDisciplina();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_tblConsultaMouseClicked

    private void tblConsultaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblConsultaKeyPressed
        try {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                carregarDisciplina();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_tblConsultaKeyPressed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        try {
            carregarDisciplina();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        try {
            excluir();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnConsultaProfessorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnConsultaProfessorMouseClicked
        try {
            if (InicioGUI.formProfessorCadastro == null || InicioGUI.formProfessorCadastro.isClosed()) {
                InicioGUI.formProfessorCadastro = new ProfessorCadastroGUI(desktop, this);
                desktop.add(InicioGUI.formProfessorCadastro);
                InicioGUI.formProfessorCadastro.setPosicao();
            }

            InicioGUI.formProfessorCadastro.setVisible(true);
            InicioGUI.formProfessorCadastro.toFront();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnConsultaProfessorMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConsultaProfessor;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnNovo;
    private javax.swing.JComboBox<String> cboDiaSemana;
    private javax.swing.JComboBox<String> cboSituacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tblConsulta;
    private javax.swing.JTextField txtCodProfessor;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtNome;
    // End of variables declaration//GEN-END:variables
}
