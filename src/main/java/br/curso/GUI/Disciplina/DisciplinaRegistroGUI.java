package br.curso.GUI.Disciplina;

import br.curso.classes.JTextFieldLimit;
import br.curso.DAO.DisciplinaDAO;
import br.curso.GUI.InicioGUI;
import br.curso.GUI.Professor.ProfessorCadastroGUI;
import br.curso.VO.DiaSemana;
import br.curso.VO.Disciplina.DisciplinaVO;
import br.curso.VO.Professor.ProfessorVO;
import br.curso.VO.SituacaoCadastro;
import java.awt.Dimension;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;

/**
 *
 * @author felipe.bruno
 */
public class DisciplinaRegistroGUI extends javax.swing.JInternalFrame {
    
    DisciplinaVO oDisciplina = new DisciplinaVO();
    public JDesktopPane desktop = null;
    
    public DisciplinaRegistroGUI(JDesktopPane i_desktop) throws Exception {
        initComponents();
        
        desktop = i_desktop;
        
        cboSituacao.addItem(SituacaoCadastro.ATIVO.getDescricao());
        cboSituacao.addItem(SituacaoCadastro.EXCLUIDO.getDescricao());
        
        cboDiaSemana.addItem(DiaSemana.DOMINGO.getDescricao());
        cboDiaSemana.addItem(DiaSemana.SEGUNDA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.TERCA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.QUARTA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.QUINTA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.SEXTA_FEIRA.getDescricao());
        cboDiaSemana.addItem(DiaSemana.SABADO.getDescricao());
        cboDiaSemana.setSelectedIndex(DiaSemana.SEGUNDA_FEIRA.getId());
        
        txtDescricao.setDocument(new JTextFieldLimit(40));
        txtLimiteVagas.setDocument(new JTextFieldLimit(3));
        txtCargaHoraria.setDocument(new JTextFieldLimit(3));
        txtCodProfessor.setDocument(new JTextFieldLimit(3));
        txtEmenta.setDocument(new JTextFieldLimit(100));
        
    }
    
    public void incluir() throws Exception {
        oDisciplina = new DisciplinaVO();
        
        txtCodigo.setText("");
        txtCodigo.setEnabled(false);
        txtDescricao.setText("");
        txtEmenta.setText("");
        txtLimiteVagas.setText("");
        txtCargaHoraria.setText("");
        txtCodProfessor.setText("");
        txtNomeProfessor.setText("");
        cboDiaSemana.setSelectedIndex(DiaSemana.SEGUNDA_FEIRA.getId());
        cboSituacao.setSelectedIndex(SituacaoCadastro.ATIVO.getId());
        cboSituacao.setEnabled(false);
    }
    
    public void salvar() throws Exception {
        
        if (txtDescricao.getText().isEmpty()) {
            exibirAlerta("Campo nome vazio!");
            txtDescricao.requestFocus();
            
        } else if (txtEmenta.getText().isEmpty()) {
            exibirAlerta("Campo Ementa preenchido vazio!");
            txtEmenta.requestFocus();
            
        } else if (txtLimiteVagas.getText().isEmpty()) {
            exibirAlerta("Limite de Vagas vazio!");
            txtLimiteVagas.requestFocus();
            
        } else if (txtCargaHoraria.getText().isEmpty()) {
            exibirAlerta("Carga Hor�ria vazia!");
            txtCargaHoraria.requestFocus();
            
        } else if (txtCodProfessor.getText().isEmpty()) {
            exibirAlerta("Carga Hor�ria vazia!");
            txtCodProfessor.requestFocus();
            
        } else {
            try {
                
                oDisciplina = new DisciplinaVO();
                
                oDisciplina.setCodigo(txtCodigo.getText().isEmpty() ? 0 : Integer.parseInt(txtCodigo.getText()));
                oDisciplina.setDescricao(txtDescricao.getText().toUpperCase());
                oDisciplina.setEmenta(txtEmenta.getText());
                oDisciplina.setLimiteVagas(Integer.parseInt(txtLimiteVagas.getText()));
                oDisciplina.setCargaHoraria(Integer.parseInt(txtCargaHoraria.getText()));
                oDisciplina.setIdProfessor(Integer.parseInt(txtCodProfessor.getText()));
                oDisciplina.setDiaSemana(cboDiaSemana.getSelectedItem().toString());
                oDisciplina.setIdSituacaoCadastro(cboSituacao.getSelectedIndex());
                
                new DisciplinaDAO().salvar(oDisciplina);
                
                JOptionPane.showMessageDialog(this, "Disciplina salva com sucesso!", title, JOptionPane.PLAIN_MESSAGE);
                incluir();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void carregar(int i_id) throws Exception {
        oDisciplina = new DisciplinaDAO().carregar(i_id);
        
        if (oDisciplina.getCodigo() != 0) {
            txtCodigo.setText(String.format("%04d", oDisciplina.getCodigo()));
            txtDescricao.setText(oDisciplina.getDescricao());
            txtEmenta.setText(oDisciplina.getEmenta());
            txtCargaHoraria.setText(String.valueOf(oDisciplina.getCargaHoraria()));
            cboSituacao.setSelectedIndex(oDisciplina.getIdSituacaoCadastro());
            txtLimiteVagas.setText(String.valueOf(oDisciplina.getLimiteVagas()));
            txtCodProfessor.setText(String.valueOf(oDisciplina.getIdProfessor()));
            cboDiaSemana.setSelectedItem(oDisciplina.getDiaSemana());
            
            cboSituacao.setEnabled(true);
        }
    }
    
    public void setProfessor(ProfessorVO i_oProfessor) throws Exception {
        txtCodProfessor.setText(String.valueOf(i_oProfessor.getCodigo()));
        txtNomeProfessor.setText(i_oProfessor.getNome());
    }
    
    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }
    
    private void exibirAlerta(String i_mensagem) {
        JOptionPane.showMessageDialog(this, i_mensagem, title, JOptionPane.WARNING_MESSAGE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnNovo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtDescricao = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cboSituacao = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtEmenta = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        cboDiaSemana = new javax.swing.JComboBox<>();
        btnConsultaProfessor = new javax.swing.JButton();
        txtNomeProfessor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtLimiteVagas = new javax.swing.JTextField();
        txtCargaHoraria = new javax.swing.JTextField();
        txtCodProfessor = new javax.swing.JTextField();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Registro Disciplina");

        jToolBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jToolBar1.setRollover(true);

        btnNovo.setText("Novo");
        btnNovo.setFocusable(false);
        btnNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        jToolBar1.add(btnNovo);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("C�digo");

        txtCodigo.setEnabled(false);

        txtDescricao.setColumns(80);

        jLabel2.setText("Descri��o");

        jLabel3.setText("Carga Hor�ria");

        jLabel6.setText("Situa��o");

        jLabel4.setText("Limite Vagas");

        jLabel5.setText("Ementa");

        txtEmenta.setColumns(100);
        txtEmenta.setRows(5);
        jScrollPane1.setViewportView(txtEmenta);

        jLabel7.setText("Dia Semana");

        btnConsultaProfessor.setIcon(new javax.swing.ImageIcon("C:\\git\\provaPL\\vrcursopl\\src\\img\\outline_search_lupa_find-512.png")); // NOI18N
        btnConsultaProfessor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnConsultaProfessorMouseClicked(evt);
            }
        });

        txtNomeProfessor.setEditable(false);

        jLabel8.setText("Professor");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(cboDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(cboSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel5)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLimiteVagas))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(txtCargaHoraria))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtCodProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnConsultaProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNomeProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel8)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 416, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addGap(1, 1, 1)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel8))
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtNomeProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnConsultaProfessor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtLimiteVagas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCargaHoraria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCodProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboSituacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            salvar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        try {
            dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        try {
            incluir();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnConsultaProfessorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnConsultaProfessorMouseClicked
        try {
            if (InicioGUI.formProfessorCadastro == null || InicioGUI.formProfessorCadastro.isClosed()) {
                InicioGUI.formProfessorCadastro = new ProfessorCadastroGUI(desktop, this);
                desktop.add(InicioGUI.formProfessorCadastro);
                InicioGUI.formProfessorCadastro.setPosicao();
            }
            
            InicioGUI.formProfessorCadastro.setVisible(true);
            InicioGUI.formProfessorCadastro.toFront();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnConsultaProfessorMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConsultaProfessor;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cboDiaSemana;
    private javax.swing.JComboBox<String> cboSituacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtCargaHoraria;
    private javax.swing.JTextField txtCodProfessor;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtDescricao;
    private javax.swing.JTextArea txtEmenta;
    private javax.swing.JTextField txtLimiteVagas;
    private javax.swing.JTextField txtNomeProfessor;
    // End of variables declaration//GEN-END:variables
}
