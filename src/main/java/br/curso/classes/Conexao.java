package br.curso.classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author felipe.bruno
 */
public class Conexao {

    public static String ip = "127.0.0.1";
    public static String porta = "8745";
    public static String nomeBanco = "curso";
    public static String usuarioBanco = "postgres";
    public static String senhaBanco = "postgres";

    private static Connection con;

    public static void conectar() throws Exception {

        try {
            con = DriverManager.getConnection("jdbc:postgresql://" + ip + ":" + porta + "/" + nomeBanco, usuarioBanco, senhaBanco);
        } catch (SQLException ex) {
            try {
                con = DriverManager.getConnection("jdbc:postgresql://" + ip + ":" + porta + "/" + "postgres", usuarioBanco, senhaBanco);

                if (!con.isClosed()) {
                    if (!CriarTabelas.bancoExiste(nomeBanco)) {
                        CriarTabelas.criarBanco(nomeBanco);

                        try {
                            con.close();
                            con = DriverManager.getConnection("jdbc:postgresql://" + ip + ":" + porta + "/" + nomeBanco, usuarioBanco, senhaBanco);

                        } catch (SQLException e3) {
                        }
                    }
                }

            } catch (Exception e) {
            }
        }
    }

    public static Connection getConexao() {
        return con;
    }

    public static Statement statement() throws Exception {
        return con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
    }
}
