package br.curso.classes;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author felipe.bruno
 */
public class CriarTabelas {

    public void criarTabelas() throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {

            stm = Conexao.statement();

            if (!tabelaExiste("situacaocadastro")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE situacaocadastro(");
                sql.append("	id serial NOT NULL,");
                sql.append("	descricao varchar(10) NOT NULL,");
                sql.append("	CONSTRAINT pk_id_situacaocadastro PRIMARY KEY (id)");
                sql.append(");");

                stm.execute(sql.toString());

                stm.execute("INSERT INTO situacaocadastro(id, descricao) VALUES (0, 'ATIVO');");
                stm.execute("INSERT INTO situacaocadastro(id, descricao) VALUES (1, 'EXCLUIDO');");

            }

            if (!tabelaExiste("titulos")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE titulos (");
                sql.append("	id serial NOT NULL,");
                sql.append("	descricao varchar(30),");
                sql.append("	CONSTRAINT pk_id_titulos PRIMARY KEY (id)");
                sql.append(");");

                stm.execute(sql.toString());

                stm.execute("INSERT INTO titulos(id, descricao) VALUES (0, 'TECNOLOGO');");
                stm.execute("INSERT INTO titulos(id, descricao) VALUES (1, 'BACHARELADO');");
                stm.execute("INSERT INTO titulos(id, descricao) VALUES (2, 'LICENCIATURA');");
                stm.execute("INSERT INTO titulos(id, descricao) VALUES (3, 'MESTRADO');");
                stm.execute("INSERT INTO titulos(id, descricao) VALUES (4, 'DOUTORADO');");
            }

            if (!tabelaExiste("professor")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE professor (");
                sql.append("	id serial NOT NULL,");
                sql.append("	nome varchar(40) NOT NULL,");
                sql.append("	rg varchar(10) NOT NULL,");
                sql.append("	cpf varchar(11) NOT NULL,");
                sql.append("	id_titulo integer NOT NULL,");
                sql.append("    id_situacaocadastro integer NOT NULL,");
                sql.append("	CONSTRAINT pk_id_professor PRIMARY KEY (id),");
                sql.append("	CONSTRAINT fk_id_titulo FOREIGN KEY (id_titulo) REFERENCES titulos (id), ");
                sql.append("    CONSTRAINT fk_id_situacaocadastro FOREIGN KEY (id_situacaocadastro) REFERENCES situacaocadastro (id)");
                sql.append(");");

                stm.execute(sql.toString());
            }

            if (!tabelaExiste("aluno")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE aluno (");
                sql.append("	id serial NOT NULL,");
                sql.append("	matricula varchar(13) NOT NULL,");
                sql.append("	nome varchar(40) NOT NULL,");
                sql.append("	rg varchar(10) NOT NULL,");
                sql.append("	cpf varchar(11) NOT NULL,");
                sql.append("    id_situacaocadastro integer NOT NULL,");
                sql.append("	CONSTRAINT pk_id_aluno PRIMARY KEY (id),");
                sql.append("    CONSTRAINT fk_id_situacaocadastro FOREIGN KEY (id_situacaocadastro) REFERENCES situacaocadastro (id)");
                sql.append(");");

                stm.execute(sql.toString());
            }

            if (!tabelaExiste("curso")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE curso (");
                sql.append("	id serial NOT NULL,");
                sql.append("	descricao varchar(40) NOT NULL,");
                sql.append("	duracao TIME NOT NULL,");
                sql.append("	periodo varchar(10) NOT NULL,");
                sql.append("	qtd_alunos integer,");
                sql.append("	carga_horaria integer,");
                sql.append("    id_situacaocadastro integer NOT NULL,");
                sql.append("	CONSTRAINT pk_id_curso PRIMARY KEY(id),");
                sql.append("    CONSTRAINT fk_id_situacaocadastro FOREIGN KEY (id_situacaocadastro) REFERENCES situacaocadastro (id)");
                sql.append(");");

                stm.execute(sql.toString());
            }

            if (!tabelaExiste("disciplina")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE disciplina (");
                sql.append("	id serial NOT NULL,");
                sql.append("	descricao varchar(40) NOT NULL,");
                sql.append("	ementa TEXT,");
                sql.append("	limite_vagas integer NOT NULL,");
                sql.append("	id_professor integer NOT NULL,");
                sql.append("	dia_semana varchar(13) NOT NULL,");
                sql.append("	carga_horaria integer,");
                sql.append("    id_situacaocadastro integer NOT NULL,");
                sql.append("	CONSTRAINT pk_id_disciplina PRIMARY KEY (id),");
                sql.append("	CONSTRAINT fk_id_professor FOREIGN KEY (id_professor) REFERENCES professor (id),");
                sql.append("    CONSTRAINT fk_id_situacaocadastro FOREIGN KEY (id_situacaocadastro) REFERENCES situacaocadastro (id)");
                sql.append(");");

                stm.execute(sql.toString());
            }

            if (!tabelaExiste("cursodisciplina")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE cursodisciplina(");
                sql.append("	id serial NOT NULL,");
                sql.append("	id_disciplina integer NOT NULL,");
                sql.append("	id_curso integer NOT NULL,");
                sql.append("	CONSTRAINT pk_id_cursodisciplina PRIMARY KEY (id),");
                sql.append("	CONSTRAINT fk_id_cursodisciplina FOREIGN KEY (id_disciplina) REFERENCES disciplina (id),");
                sql.append("	CONSTRAINT fk_id_curso FOREIGN KEY (id_curso) REFERENCES curso (id)");
                sql.append(");");

                stm.execute(sql.toString());
            }

            if (!tabelaExiste("cursoaluno")) {
                sql = new StringBuilder();
                sql.append("CREATE TABLE cursoaluno(");
                sql.append("	id serial NOT NULL,");
                sql.append("	id_aluno integer NOT NULL,");
                sql.append("	id_curso integer NOT NULL,");
                sql.append("	CONSTRAINT pk_id_cursoaluno PRIMARY KEY (id),");
                sql.append("	CONSTRAINT fk_id_cursoaluno FOREIGN KEY (id_aluno) REFERENCES aluno (id),");
                sql.append("	CONSTRAINT fk_id_curso FOREIGN KEY (id_curso) REFERENCES curso (id)");
                sql.append(");");

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }

            if (rst != null) {
                rst.close();
            }
        }
    }

    public static boolean tabelaExiste(String i_tabela) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;
        boolean tabelaExiste = false;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT EXISTS (");
        sql.append("	SELECT 1");
        sql.append("	FROM pg_catalog.pg_class c");
        sql.append("	JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace");

        if (i_tabela.contains(".")) {
            String[] tabela = i_tabela.split(".");
            sql.append("	WHERE n.nspname = '" + tabela[0] + "'");
            sql.append("	AND c.relname = '" + tabela[1] + "'");

        } else {
            sql.append("	WHERE n.nspname = 'public'");
            sql.append("	AND c.relname = '" + i_tabela + "'");

        }

        sql.append("	AND c.relkind = 'r'");
        sql.append(");");

        rst = stm.executeQuery(sql.toString());

        if (rst.next()) {
            tabelaExiste = rst.getBoolean("exists");
        }

        stm.close();

        return tabelaExiste;
    }

    public static boolean schemaExiste(String i_schema) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;
        boolean schemaExiste = false;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT EXISTS");
        sql.append("  ( SELECT 1");
        sql.append("   FROM pg_catalog.pg_class c");
        sql.append("   JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace");
        sql.append("   WHERE n.nspname = '" + i_schema + "'");
        sql.append(");");

        rst = stm.executeQuery(sql.toString());

        if (rst.next()) {
            schemaExiste = rst.getBoolean("exists");
        }

        stm.close();

        return schemaExiste;
    }

    public static boolean bancoExiste(String i_nomeBanco) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;
        boolean bancoExiste = false;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT EXISTS");
        sql.append("  (SELECT 1");
        sql.append("   FROM pg_database");
        sql.append("   WHERE datname LIKE '" + i_nomeBanco + "'");
        sql.append(");");

        rst = stm.executeQuery(sql.toString());

        if (rst.next()) {
            bancoExiste = rst.getBoolean("exists");
        }

        stm.close();

        return bancoExiste;
    }

    public static void criarBanco(String i_nomeBanco) throws Exception {
        try (Statement stm = Conexao.statement()) {
            stm.execute("CREATE DATABASE curso;");
        }

    }
}
