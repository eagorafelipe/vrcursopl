package br.curso.DAO;

import br.curso.VO.Aluno.AlunoVO;
import br.curso.VO.Curso.CursoFiltroVO;
import br.curso.VO.Curso.CursoVO;
import br.curso.VO.Disciplina.DisciplinaVO;
import br.curso.VO.SituacaoCadastro;
import br.curso.classes.Conexao;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author felipe.bruno
 */
public class CursoDAO {

    public ArrayList<CursoVO> consultar(CursoFiltroVO i_oFiltro) throws Exception {
        ArrayList<CursoVO> vCurso = new ArrayList();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT c.*, sc.descricao AS situacao");
        sql.append(" FROM curso c");
        sql.append(" INNER JOIN situacaocadastro sc ON sc.id = c.id_situacaocadastro");
        sql.append(" WHERE 1 =1");

        if (i_oFiltro.id > 0) {
            sql.append(" AND c.id = " + i_oFiltro.id);
        }

        if (!i_oFiltro.descricao.isEmpty()) {
            sql.append(" AND c.descricao ILIKE '%" + i_oFiltro.descricao + "%'");
        }

        if (!i_oFiltro.periodo.equals("TODOS")) {
            sql.append(" AND c.periodo = '" + i_oFiltro.periodo + "'");
        }

        if (!i_oFiltro.situacaoCadastro.equals("TODOS")) {
            sql.append(" AND sc.descricao = '" + i_oFiltro.situacaoCadastro + "'");
        }

        rst = stm.executeQuery(sql.toString());

        while (rst.next()) {
            CursoVO oCurso = new CursoVO();
            oCurso.setCodigo(rst.getInt("id"));
            oCurso.setDescricao(rst.getString("descricao"));
            oCurso.setDuracao(rst.getString("duracao"));
            oCurso.setPeriodo(rst.getString("periodo"));
            oCurso.setQtdAlunos(rst.getInt("qtd_alunos"));
            oCurso.setCargaHoraria(rst.getInt("carga_horaria"));
            oCurso.setSituacaoCadastro(rst.getString("situacao"));

            vCurso.add(oCurso);
        }

        return vCurso;
    }

    public void salvar(CursoVO oCurso) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM curso WHERE id = " + oCurso.getCodigo());

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE curso SET");
                sql.append(" descricao = '" + oCurso.getDescricao() + "',");
                sql.append(" duracao = '" + oCurso.getDuracao() + "',");
                sql.append(" periodo = '" + oCurso.getPeriodo() + "',");
                sql.append(" qtd_alunos = " + oCurso.getQtdAlunos() + ",");
                sql.append(" carga_horaria = " + oCurso.getCargaHoraria());
                sql.append(" WHERE id = " + rst.getInt("id"));

                stm.execute(sql.toString());

                stm.execute("DELETE FROM cursodisciplina WHERE id_curso = " + oCurso.getCodigo());

                for (DisciplinaVO oDisciplina : oCurso.getvDisciplina()) {
                    sql = new StringBuilder();
                    sql.append("INSERT INTO cursodisciplina(id_disciplina, id_curso) VALUES (");
                    sql.append(oDisciplina.getCodigo() + ", ");
                    sql.append(oCurso.getCodigo());
                    sql.append(");");

                    stm.execute(sql.toString());
                }

                stm.execute("DELETE FROM cursoaluno WHERE id_curso = " + oCurso.getCodigo());

                for (AlunoVO oAluno : oCurso.getvAluno()) {
                    sql = new StringBuilder();
                    sql.append("INSERT INTO cursoaluno(id_aluno, id_curso) VALUES (");
                    sql.append(oAluno.getCodigo() + ", ");
                    sql.append(oCurso.getCodigo());
                    sql.append(");");

                    stm.execute(sql.toString());
                }

            } else {
                rst = stm.executeQuery("SELECT COALESCE(MAX(id), 0) + 1 AS codigo FROM curso;");
                rst.next();

                oCurso.setCodigo(rst.getInt("codigo"));

                sql = new StringBuilder();
                sql.append("INSERT INTO curso (id, descricao, duracao, periodo, qtd_alunos, carga_horaria, id_situacaocadastro) VALUES ( ");
                sql.append("" + oCurso.getCodigo() + ", ");
                sql.append("'" + oCurso.getDescricao() + "', ");
                sql.append("'" + oCurso.getDuracao() + "', ");
                sql.append("'" + oCurso.getPeriodo() + "', ");
                sql.append(oCurso.getQtdAlunos() + ", ");
                sql.append(oCurso.getCargaHoraria() + ", ");
                sql.append(SituacaoCadastro.ATIVO.getId());
                sql.append(");");

                stm.execute(sql.toString());

                for (DisciplinaVO oDisciplina : oCurso.getvDisciplina()) {
                    sql = new StringBuilder();
                    sql.append("INSERT INTO cursodisciplina(id_disciplina, id_curso) VALUES (");
                    sql.append(oDisciplina.getCodigo() + ", ");
                    sql.append(oCurso.getCodigo());
                    sql.append(");");

                    stm.execute(sql.toString());
                }

                for (AlunoVO oAluno : oCurso.getvAluno()) {
                    sql = new StringBuilder();
                    sql.append("INSERT INTO cursoaluno(id_aluno, id_curso) VALUES (");
                    sql.append(oAluno.getCodigo() + ", ");
                    sql.append(oCurso.getCodigo());
                    sql.append(");");

                    stm.execute(sql.toString());
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public CursoVO carregar(int i_id) throws Exception {
        CursoVO oCurso = new CursoVO();
        Statement stm = null;
        ResultSet rst = null;
        ResultSet rstAluno = null;
        ResultSet rstDisciplina = null;
        StringBuilder sql = null;

        stm = Conexao.statement();
        sql = new StringBuilder();

        rst = stm.executeQuery("SELECT * FROM curso WHERE id = " + i_id);

        if (rst.next()) {
            oCurso.setCodigo(rst.getInt("id"));
            oCurso.setDescricao(rst.getString("descricao"));
            oCurso.setDuracao(rst.getString("duracao"));
            oCurso.setPeriodo(rst.getString("periodo"));
            oCurso.setQtdAlunos(rst.getInt("qtd_alunos"));
            oCurso.setCargaHoraria(rst.getInt("carga_horaria"));
            oCurso.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));

            sql = new StringBuilder();
            sql.append("SELECT a.id, a.nome");
            sql.append(" FROM cursoaluno ca");
            sql.append(" INNER JOIN aluno a ON a.id = ca.id_aluno");
            sql.append(" WHERE ca.id_curso = " + oCurso.getCodigo());

            rstAluno = stm.executeQuery(sql.toString());

            ArrayList<AlunoVO> vAluno = new ArrayList();

            while (rstAluno.next()) {
                AlunoVO oAluno = new AlunoVO();
                oAluno.setCodigo(rstAluno.getInt("id"));
                oAluno.setNome(rstAluno.getString("nome"));

                vAluno.add(oAluno);
            }

            oCurso.setvAluno(vAluno);

            sql = new StringBuilder();
            sql.append("SELECT d.descricao, p.nome, d.dia_semana");
            sql.append(" FROM cursodisciplina cd");
            sql.append(" INNER JOIN disciplina d ON d.id = cd.id_disciplina");
            sql.append(" INNER JOIN professor p ON p.id = d.id_professor");
            sql.append(" WHERE cd.id_curso = " + oCurso.getCodigo());

            rstDisciplina = stm.executeQuery(sql.toString());

            ArrayList<DisciplinaVO> vDisciplina = new ArrayList();

            while (rstDisciplina.next()) {
                DisciplinaVO oDisciplina = new DisciplinaVO();
                oDisciplina.setDescricao(rstDisciplina.getString("descricao"));
                oDisciplina.setProfessor(rstDisciplina.getString("nome"));
                oDisciplina.setDiaSemana(rstDisciplina.getString("dia_semana"));

                vDisciplina.add(oDisciplina);
            }

            oCurso.setvDisciplina(vDisciplina);
        }

        return oCurso;
    }

    public void excluir(int i_id) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();
            rst = stm.executeQuery("SELECT id FROM curso WHERE id = " + i_id);

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE curso");
                sql.append(" SET id_situacaocadastro = " + SituacaoCadastro.EXCLUIDO.getId());
                sql.append(" WHERE id = " + i_id);

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public boolean getDiaSemanaDisciplina(String i_diaSemana) throws Exception {
        boolean achou = false;
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT EXISTS(");
        sql.append("	SELECT 1 FROM curso c");
        sql.append("	INNER JOIN cursodisciplina cd ON cd.id_curso = c.id");
        sql.append("	INNER JOIN disciplina d ON d.id = cd.id_disciplina");
        sql.append("	WHERE d.dia_semana = 'QUARTA-FEIRA'");
        sql.append("	LIMIT 1");
        sql.append(");");

        rst = stm.executeQuery(sql.toString());
        rst.next();

        achou = rst.getBoolean("exists");

        return achou;
    }

    public int getCargaHorariaCurso(int i_id) throws Exception {
        int cargaHoraria = 0;
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT SUM(d.carga_horaria) carcaga_horaria");
        sql.append(" FROM disciplina d");
        sql.append(" LEFT JOIN cursodisciplina cd ON cd.id_disciplina = d.id");
        sql.append(" INNER JOIN curso c ON c.id = cd.id_curso");
        sql.append(" WHERE c.id = " + i_id);

        rst = stm.executeQuery(sql.toString());

        if (rst.next()) {
            cargaHoraria = rst.getInt("carcaga_horaria");
        }

        return cargaHoraria;
    }

}
