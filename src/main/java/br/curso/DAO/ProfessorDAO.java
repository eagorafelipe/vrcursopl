package br.curso.DAO;

import br.curso.VO.Professor.ProfessorFiltroVO;
import br.curso.VO.Professor.ProfessorVO;
import br.curso.VO.SituacaoCadastro;
import br.curso.classes.Conexao;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author felipe.bruno
 */
public class ProfessorDAO {

    public ArrayList<ProfessorVO> consultar(ProfessorFiltroVO i_oFiltro) throws Exception {
        ArrayList<ProfessorVO> vProfessor = new ArrayList<>();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT p.*, t.descricao AS titulo, sc.descricao AS situacao");
        sql.append(" FROM professor p");
        sql.append(" INNER JOIN titulos t ON t.id = p.id_titulo");
        sql.append(" INNER JOIN situacaocadastro sc ON sc.id = p.id_situacaocadastro");
        sql.append(" WHERE 1 = 1");

        if (i_oFiltro.id > 0) {
            sql.append(" AND p.id = " + i_oFiltro.id);
        }

        if (!i_oFiltro.nome.isEmpty()) {
            sql.append(" AND p.nome ILIKE '%" + i_oFiltro.nome + "%'");
        }

        if (!i_oFiltro.situacaoCadastro.equals("TODOS")) {
            sql.append(" AND sc.descricao = '" + i_oFiltro.situacaoCadastro + "'");
        }

        rst = stm.executeQuery(sql.toString());

        while (rst.next()) {
            ProfessorVO oProfessor = new ProfessorVO();
            oProfessor.setCodigo(rst.getInt("id"));
            oProfessor.setNome(rst.getString("nome"));
            oProfessor.setRg(rst.getString("rg"));
            oProfessor.setCpf(rst.getString("cpf"));
            oProfessor.setIdTitulo(rst.getInt("id_titulo"));
            oProfessor.setTitulo(rst.getString("titulo"));
            oProfessor.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));
            oProfessor.setSituacaCadastro(rst.getString("situacao"));

            vProfessor.add(oProfessor);

        }

        return vProfessor;
    }

    public void salvar(ProfessorVO oProfessor) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM professor WHERE id = " + oProfessor.getCodigo());

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE professor SET");
                sql.append(" nome = '" + oProfessor.getNome() + "',");
                sql.append(" rg = '" + oProfessor.getRg() + "',");
                sql.append(" cpf = '" + oProfessor.getCpf() + "',");
                sql.append(" id_titulo = " + oProfessor.getIdTitulo() + ",");
                sql.append(" id_situacaocadastro = " + oProfessor.getIdSituacaoCadastro());
                sql.append(" WHERE id = " + rst.getInt("id"));
                sql.append(";");

                stm.execute(sql.toString());
            } else {
                sql = new StringBuilder();
                sql.append("INSERT INTO professor(nome, rg, cpf, id_titulo, id_situacaocadastro) VALUES (");
                sql.append("'" + oProfessor.getNome() + "', ");
                sql.append("'" + oProfessor.getRg() + "', ");
                sql.append("'" + oProfessor.getCpf() + "', ");
                sql.append(oProfessor.getIdTitulo() + ", ");
                sql.append(SituacaoCadastro.ATIVO.getId());
                sql.append(");");

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public ProfessorVO carregar(int i_id) throws Exception {
        ProfessorVO oProfessor = new ProfessorVO();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT * FROM professor WHERE id = " + i_id);

        rst = stm.executeQuery(sql.toString());

        if (rst.next()) {
            oProfessor.setCodigo(rst.getInt("id"));
            oProfessor.setNome(rst.getString("nome"));
            oProfessor.setRg(rst.getString("rg"));
            oProfessor.setCpf(rst.getString("cpf"));
            oProfessor.setIdTitulo(rst.getInt("id_titulo"));
            oProfessor.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));
        }

        return oProfessor;
    }

    public void excluir(int i_id) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM professor WHERE id = " + i_id);

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE professor");
                sql.append(" SET id_situacaocadastro = " + SituacaoCadastro.EXCLUIDO.getId());
                sql.append(" WHERE id = " + i_id);

                stm.execute(sql.toString());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

}
