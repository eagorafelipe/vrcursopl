package br.curso.DAO;

import br.curso.VO.Disciplina.DisciplinaFiltroVO;
import br.curso.VO.Disciplina.DisciplinaVO;
import br.curso.VO.SituacaoCadastro;
import br.curso.classes.Conexao;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author felipe.bruno
 */
public class DisciplinaDAO {

    public ArrayList<DisciplinaVO> consultar(DisciplinaFiltroVO i_oFiltro) throws Exception {
        ArrayList<DisciplinaVO> vDisciplina = new ArrayList<>();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT d.*, sc.descricao AS situacao, p.nome AS professor");
        sql.append(" FROM disciplina d");
        sql.append(" INNER JOIN situacaocadastro sc ON sc.id = d.id_situacaocadastro");
        sql.append(" INNER JOIN professor p ON p.id = d.id_professor");
        sql.append(" WHERE 1 = 1");

        if (i_oFiltro.id > 0) {
            sql.append(" AND d.id = " + i_oFiltro.id);
        }

        if (!i_oFiltro.descricao.isEmpty()) {
            sql.append(" AND d.descricao ILIKE '%");
        }

        if (!i_oFiltro.diaSemana.equals("TODOS")) {
            sql.append(" AND d.dia_semana = '" + i_oFiltro.diaSemana + "'");

        }

        if (i_oFiltro.idProfessor > 0) {
            sql.append(" AND d.id_professor = " + i_oFiltro.idProfessor);
        }

        if (!i_oFiltro.situacaoCadastro.equals("TODOS")) {
            sql.append(" AND sc.descricao = '" + i_oFiltro.situacaoCadastro + "'");
        }

        rst = stm.executeQuery(sql.toString());

        while (rst.next()) {
            DisciplinaVO oDisciplina = new DisciplinaVO();
            oDisciplina.setCodigo(rst.getInt("id"));
            oDisciplina.setDescricao(rst.getString("descricao"));
            oDisciplina.setEmenta(rst.getString("ementa"));
            oDisciplina.setLimiteVagas(rst.getInt("limite_vagas"));
            oDisciplina.setIdProfessor(rst.getInt("id_professor"));
            oDisciplina.setProfessor(rst.getString("professor"));
            oDisciplina.setDiaSemana(rst.getString("dia_semana"));
            oDisciplina.setCargaHoraria(rst.getInt("carga_horaria"));
            oDisciplina.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));
            oDisciplina.setSituacaoCadastro(rst.getString("situacao"));

            vDisciplina.add(oDisciplina);
        }

        return vDisciplina;
    }

    public void salvar(DisciplinaVO oDisciplina) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM disciplina WHERE id = " + oDisciplina.getCodigo());

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE disciplina SET");
                sql.append(" descricao = '" + oDisciplina.getDescricao() + "',");
                sql.append(" ementa = '" + oDisciplina.getEmenta() + "',");
                sql.append(" limite_vagas = " + oDisciplina.getLimiteVagas() + ",");
                sql.append(" id_professor = " + oDisciplina.getIdProfessor() + ",");
                sql.append(" dia_semana = '" + oDisciplina.getDiaSemana() + "',");
                sql.append(" carga_horaria = " + oDisciplina.getCargaHoraria() + ",");
                sql.append(" id_situacaocadastro = " + oDisciplina.getIdSituacaoCadastro());
                sql.append(" WHERE id = " + rst.getInt("id"));

                stm.execute(sql.toString());

            } else {
                sql = new StringBuilder();
                sql.append("INSERT INTO disciplina(descricao, ementa, limite_vagas, dia_semana, carga_horaria, id_professor, id_situacaocadastro) VALUES (");
                sql.append("'" + oDisciplina.getDescricao() + "', ");
                sql.append("'" + oDisciplina.getEmenta() + "', ");
                sql.append(oDisciplina.getLimiteVagas() + ", ");
                sql.append("'" + oDisciplina.getDiaSemana() + "', ");
                sql.append(oDisciplina.getCargaHoraria() + ", ");
                sql.append(oDisciplina.getIdProfessor() + ", ");
                sql.append(SituacaoCadastro.ATIVO.getId());
                sql.append(");");

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public DisciplinaVO carregar(int i_id) throws Exception {
        DisciplinaVO oDisciplina = new DisciplinaVO();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();

        rst = stm.executeQuery("SELECT * FROM disciplina WHERE id = " + i_id);

        if (rst.next()) {
            oDisciplina.setCodigo(rst.getInt("id"));
            oDisciplina.setDescricao(rst.getString("descricao"));
            oDisciplina.setEmenta(rst.getString("ementa"));
            oDisciplina.setLimiteVagas(rst.getInt("limite_vagas"));
            oDisciplina.setIdProfessor(rst.getInt("id_professor"));
            oDisciplina.setDiaSemana(rst.getString("dia_semana"));
            oDisciplina.setCargaHoraria(rst.getInt("carga_horaria"));
            oDisciplina.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));
        }

        return oDisciplina;
    }

    public void excluir(int i_id) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM disciplina WHERE id = " + i_id);

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE disciplina");
                sql.append(" SET id_situacaocadastro = " + SituacaoCadastro.EXCLUIDO.getId());
                sql.append(" WHERE id = " + i_id);

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

}
