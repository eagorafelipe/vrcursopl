package br.curso.DAO;

import br.curso.VO.Aluno.AlunoFiltroVO;
import br.curso.VO.Aluno.AlunoVO;
import br.curso.VO.SituacaoCadastro;
import br.curso.classes.Conexao;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author felipe.bruno
 */
public class AlunoDAO {

    public ArrayList<AlunoVO> consultar(AlunoFiltroVO i_oFiltro) throws Exception {
        ArrayList<AlunoVO> vAluno = new ArrayList<>();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();
        sql.append("SELECT a.*, sc.descricao AS situacao");
        sql.append(" FROM aluno a");
        sql.append(" INNER JOIN situacaocadastro sc ON sc.id = a.id_situacaocadastro");
        sql.append(" WHERE 1 = 1");

        if (i_oFiltro.id > 0) {
            sql.append(" AND a.id = " + i_oFiltro.id);
        }

        if (!i_oFiltro.nome.isEmpty()) {
            sql.append(" AND a.nome ILIKE '%" + i_oFiltro.nome + "%'");
        }

        if (!i_oFiltro.situacaoCadastro.equals("TODOS")) {
            sql.append(" AND sc.descricao = '" + i_oFiltro.situacaoCadastro + "'");
        }

        rst = stm.executeQuery(sql.toString());

        while (rst.next()) {
            AlunoVO oAluno = new AlunoVO();
            oAluno.setCodigo(rst.getInt("id"));
            oAluno.setMatricula(rst.getString("matricula"));
            oAluno.setNome(rst.getString("nome"));
            oAluno.setRg(rst.getString("rg"));
            oAluno.setCpf(rst.getString("cpf"));
            oAluno.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));
            oAluno.setSituacaoCadastro(rst.getString("situacao"));

            vAluno.add(oAluno);
        }

        return vAluno;
    }

    public void salvar(AlunoVO oAluno) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM aluno WHERE id = " + oAluno.getCodigo());

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE aluno SET");
                sql.append(" matricula = '" + oAluno.getMatricula() + "', ");
                sql.append(" nome = '" + oAluno.getNome() + "', ");
                sql.append(" rg = '" + oAluno.getRg() + "', ");
                sql.append(" cpf = '" + oAluno.getCpf() + "', ");
                sql.append(" id_situacaocadastro = " + oAluno.getIdSituacaoCadastro());
                sql.append(" WHERE id = " + rst.getInt("id"));
                sql.append(";");

                stm.execute(sql.toString());

            } else {
                sql = new StringBuilder();
                sql.append("INSERT INTO aluno(matricula, nome, rg, cpf, id_situacaocadastro) VALUES (");
                sql.append("'" + oAluno.getMatricula() + "', ");
                sql.append("'" + oAluno.getNome() + "', ");
                sql.append("'" + oAluno.getRg() + "', ");
                sql.append("'" + oAluno.getCpf() + "', ");
                sql.append(SituacaoCadastro.ATIVO.getId());
                sql.append(");");

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

    public AlunoVO carregar(int i_id) throws Exception {
        AlunoVO oAluno = new AlunoVO();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        stm = Conexao.statement();

        sql = new StringBuilder();

        rst = stm.executeQuery("SELECT * FROM aluno WHERE id = " + i_id);

        if (rst.next()) {
            oAluno.setCodigo(rst.getInt("id"));
            oAluno.setMatricula(rst.getString("matricula"));
            oAluno.setNome(rst.getString("nome"));
            oAluno.setRg(rst.getString("rg"));
            oAluno.setCpf(rst.getString("cpf"));
            oAluno.setIdSituacaoCadastro(rst.getInt("id_situacaocadastro"));
        }

        return oAluno;
    }

    public void excluir(int i_id) throws Exception {
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = null;

        try {
            stm = Conexao.statement();

            rst = stm.executeQuery("SELECT id FROM aluno WHERE id = " + i_id);

            if (rst.next()) {
                sql = new StringBuilder();
                sql.append("UPDATE aluno");
                sql.append(" SET id_situacaocadastro = " + SituacaoCadastro.EXCLUIDO.getId());
                sql.append(" WHERE id = " + i_id);

                stm.execute(sql.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (stm != null) {
                stm.close();
            }
        }
    }

}
