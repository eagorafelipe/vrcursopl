package br.curso.VO;

/**
 *
 * @author felipe.bruno
 */
public enum SituacaoCadastro {

    ATIVO(0, "ATIVO"),
    EXCLUIDO(1, "EXCLU�DO");

    private int id = 0;
    private String descricao = "";

    private SituacaoCadastro(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
}
