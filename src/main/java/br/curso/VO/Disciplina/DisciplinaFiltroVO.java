package br.curso.VO.Disciplina;

/**
 *
 * @author felipe.bruno
 */
public class DisciplinaFiltroVO {

    public int id = -1;
    public String descricao = "";
    public String diaSemana = "";
    public int idProfessor = -1;
    public String situacaoCadastro = "";
}
