package br.curso.VO;

/**
 *
 * @author felipe.bruno
 */
public enum DiaSemana {
    DOMINGO(0, "DOMINGO"),
    SEGUNDA_FEIRA(1, "SEGUNDA-FEIRA"),
    TERCA_FEIRA(2, "TERCA-FEIRA"),
    QUARTA_FEIRA(3, "QUARTA-FEIRA"),
    QUINTA_FEIRA(4, "QUINTA-FEIRA"),
    SEXTA_FEIRA(5, "SEXTA-FEIRA"),
    SABADO(6, "SABADO");

    private int id = 0;
    private String descricao = "";

    private DiaSemana(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return this.id;
    }

    public String getDescricao() {
        return this.descricao;
    }

}
