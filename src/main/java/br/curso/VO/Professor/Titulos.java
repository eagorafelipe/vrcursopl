package br.curso.VO.Professor;

/**
 *
 * @author felipe.bruno
 */
public enum Titulos {
    TECNOLOGO(0, "TECNOLOGO"),
    BACHARELADO(1, "BACHARELADO"),
    LICENCIATURA(2, "LICENCIATURA"),
    MESTRADO(3, "MESTRADO"),
    DOUTORADO(4, "DOUTORADO");

    private int id = 0;
    private String descricao = "";

    private Titulos(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
}
