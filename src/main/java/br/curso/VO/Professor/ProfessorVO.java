package br.curso.VO.Professor;

/**
 *
 * @author felipe.bruno
 */
public class ProfessorVO {

    private int codigo = -1;
    private String nome = "";
    private String rg = "";
    private String cpf = "";
    private String titulo = "";
    private int idTitulo = -1;
    private int idSituacaoCadastro  =-1;
    private String situacaCadastro = "";
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIdTitulo() {
        return idTitulo;
    }

    public void setIdTitulo(int idTitulo) {
        this.idTitulo = idTitulo;
    }

    public int getIdSituacaoCadastro() {
        return idSituacaoCadastro;
    }

    public void setIdSituacaoCadastro(int idSituacaoCadastro) {
        this.idSituacaoCadastro = idSituacaoCadastro;
    }

    public String getSituacaCadastro() {
        return situacaCadastro;
    }

    public void setSituacaCadastro(String situacaCadastro) {
        this.situacaCadastro = situacaCadastro;
    }
    
    

}
