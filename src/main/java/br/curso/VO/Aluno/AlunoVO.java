package br.curso.VO.Aluno;

/**
 *
 * @author felipe.bruno
 */
public class AlunoVO {

    private int codigo = -1;
    private String matricula = "";
    private String nome = "";
    private String rg = "";
    private String cpf = "";
    private int idSituacaoCadastro = -1;
    private String SituacaoCadastro = "";

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdSituacaoCadastro() {
        return idSituacaoCadastro;
    }

    public void setIdSituacaoCadastro(int idSituacaoCadastro) {
        this.idSituacaoCadastro = idSituacaoCadastro;
    }

    public String getSituacaoCadastro() {
        return SituacaoCadastro;
    }

    public void setSituacaoCadastro(String SituacaoCadastro) {
        this.SituacaoCadastro = SituacaoCadastro;
    }

}
