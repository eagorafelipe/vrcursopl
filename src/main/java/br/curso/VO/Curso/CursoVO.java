package br.curso.VO.Curso;

import br.curso.VO.Aluno.AlunoVO;
import br.curso.VO.Disciplina.DisciplinaVO;
import java.util.ArrayList;

/**
 *
 * @author felipe.bruno
 */
public class CursoVO {

    private int codigo = -1;
    private String descricao = "";
    private String duracao = "";
    private String periodo = "";
    private int qtdAlunos = -1;
    private int cargaHoraria = -1;
    private int idSituacaoCadastro = -1;
    private String situacaoCadastro = "";
    private ArrayList<DisciplinaVO> vDisciplina = new ArrayList();
    private ArrayList<AlunoVO> vAluno = new ArrayList();

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDuracao() {
        return duracao;
    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public int getQtdAlunos() {
        return qtdAlunos;
    }

    public void setQtdAlunos(int qtdAlunos) {
        this.qtdAlunos = qtdAlunos;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public int getIdSituacaoCadastro() {
        return idSituacaoCadastro;
    }

    public void setIdSituacaoCadastro(int idSituacaoCadastro) {
        this.idSituacaoCadastro = idSituacaoCadastro;
    }

    public String getSituacaoCadastro() {
        return situacaoCadastro;
    }

    public void setSituacaoCadastro(String situacaoCadastro) {
        this.situacaoCadastro = situacaoCadastro;
    }

    public ArrayList<DisciplinaVO> getvDisciplina() {
        return vDisciplina;
    }

    public void setvDisciplina(ArrayList<DisciplinaVO> vDisciplina) {
        this.vDisciplina = vDisciplina;
    }

    public ArrayList<AlunoVO> getvAluno() {
        return vAluno;
    }

    public void setvAluno(ArrayList<AlunoVO> vAluno) {
        this.vAluno = vAluno;
    }

}
