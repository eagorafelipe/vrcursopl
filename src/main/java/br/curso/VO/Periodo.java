package br.curso.VO;

/**
 *
 * @author felipe.bruno
 */
public enum Periodo {
    MATUTINO(0, "MATUTINO"),
    VESPERTINO(1, "VESPERTINO"),
    NOTURNO(2, "NOTURNO"),
    INTEGRAL(3, "INTEGRAL");

    private int id = 0;
    private String descricao = "";

    private Periodo(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return this.id;
    }

    public String getDescricao() {
        return this.descricao;
    }
}
