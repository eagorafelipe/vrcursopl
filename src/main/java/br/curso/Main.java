package br.curso;

import br.curso.classes.Conexao;
import br.curso.classes.CriarTabelas;
import br.curso.GUI.InicioGUI;
import javax.swing.UIManager;

/**
 *
 * @author felipe.bruno
 */
public class Main {

    public static void main(String args[]) {
        try {
            Conexao.conectar();

            new CriarTabelas().criarTabelas();

            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            InicioGUI form = new InicioGUI();
            form.setVisible(true);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
