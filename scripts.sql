SELECT EXISTS (
	SELECT 1
	FROM pg_catalog.pg_class c
	JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
	WHERE n.nspname = 'public'
);

SELECT EXISTS (
	SELECT 1
	FROM pg_catalog.pg_class c
	JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
	WHERE c.relname = 'curso'
	AND c.relkind = 'r'
);

CREATE TABLE titulos (
	id serial NOT NULL,
	descricao varchar(50),
	CONSTRAINT fk_id_titulos PRIMARY KEY (id)
);

INSERT INTO titulos(id, descricao) VALUES (0, 'TECNOLOGO');
INSERT INTO titulos(id, descricao) VALUES (1, 'BACHARELADO');
INSERT INTO titulos(id, descricao) VALUES (2, 'LICENCIATURA');
INSERT INTO titulos(id, descricao) VALUES (3, 'MESTRADO');
INSERT INTO titulos(id, descricao) VALUES (4, 'DOUTORADO');

CREATE TABLE periodo(
	id serial NOT NULL,
	descricao varchar(5) NOT NULL,
	CONSTRAINT pk_id_periodo PRIMARY KEY (id)
);

INSERT INTO periodo(id, descricao) VALUES (0, 'MANHA');
INSERT INTO periodo(id, descricao) VALUES (1, 'TARDE');
INSERT INTO periodo(id, descricao) VALUES (2, 'NOITE');

CREATE TABLE aluno (
	id serial NOT NULL,
	matricula varchar(13) NOT NULL,
	nome varchar(80) NOT NULL,
	rg varchar(10) NOT NULL,
	cpf varchar(11) NOT NULL,
	CONSTRAINT pk_id_aluno PRIMARY KEY (id)
);

CREATE TABLE professor (
	id serial NOT NULL,
	nome varchar(80) NOT NULL,
	rg varchar(10) NOT NULL,
	cpf varchar(11) NOT NULL,
	id_titulo integer NOT NULL,
	CONSTRAINT pk_id_aluno PRIMARY KEY (id),
	CONSTRAINT fk_id_titulo FOREIGN KEY (id_titulo) REFERENCES 
);

CREATE TABLE curso (
	id serial NOT NULL,
	descricao varchar(50) NOT NULL,
	duracao TIME NOT NULL,
	periodo varchar(6) NOT NULL,
	qtd_alunos integer,
	carga_horaria integer,
	CONSTRAINT pk_id_curso PRIMARY KEY(id)
);

CREATE TABLE disciplina (
	id serial NOT NULL,
	descricao varchar(50) NOT NULL,
	ementa TEXT,
	limite_vagas integer NOT NULL,
	id_professor integer NOT NULL,
	dia_semana varchar(13) NOT NULL,
	carga_horaria integer,
	CONSTRAINT pk_id_disciplina PRIMARY KEY (id),
	CONSTRAINT fk_id_professor FOREIGN KEY (id_professor) REFERENCES professor (id)
);

INSERT INTO aluno(matricula, nome, rg, cpf, id_situacaocadastro) VALUES ('1', 'Thomas Matheus Rezende' ,'203749145', '40729047679', 0);
INSERT INTO aluno(matricula, nome, rg, cpf, id_situacaocadastro) VALUES ('2', 'Tom�s Luiz Nogueira', '264260892', '54222326151', 0);
INSERT INTO aluno(matricula, nome, rg, cpf, id_situacaocadastro) VALUES ('3', 'Renan Kevin Ara�jo', '459483195', '74871908542', 0);
INSERT INTO aluno(matricula, nome, rg, cpf, id_situacaocadastro) VALUES ('4', 'Augusto Daniel Filipe Souza', '254279223', '80865617392', 0);
INSERT INTO aluno(matricula, nome, rg, cpf, id_situacaocadastro) VALUES ('5', 'Diego Leonardo Ara�jo', '471755485', '91531762603', 0);

SELECT * FROM aluno

INSERT INTO professor(nome, rg, cpf, id_titulo, id_situacaocadastro) VALUES ('Lucas Noah Barros' ,'346486622', '56059290701', 1, 0);
INSERT INTO professor(nome, rg, cpf, id_titulo, id_situacaocadastro) VALUES ('Luiza Vera Gabriela Porto', '378352453', '16094704193', 3, 0);
INSERT INTO professor(nome, rg, cpf, id_titulo, id_situacaocadastro) VALUES ('Mateus Arthur Fernando Castro', '115892382', '98333101178', 4, 0);
INSERT INTO professor(nome, rg, cpf, id_titulo, id_situacaocadastro) VALUES ('Simone Luana Isadora Nogueira', '276460418', '88968672822', 2, 0);
INSERT INTO professor(nome, rg, cpf, id_titulo, id_situacaocadastro) VALUES ('Benedito Enrico Figueiredo', '418849985', '12146053542', 0, 0);

SELECT * FROM professor

CREATE TABLE situacaocadastro(
	id serial NOT NULL,
	id varchar(10) NOT NULL,
	CONSTRAINT pk_id_situacaocadastro PRIMARY KEY (id)
);

CREATE TABLE cursodisciplina(
	id serial NOT NULL,
	id_disciplina integer NOT NULL,
	id_curso integer NOT NULL,
	CONSTRAINT pk_id_cursodisciplina PRIMARY KEY (id),
	CONSTRAINT fk_id_cursodisciplina FOREIGN KEY (id_disciplina) REFERENCES disciplina (id),
	CONSTRAINT fk_id_curso FOREIGN KEY (id_curso) REFERENCES curso (id)
);

CREATE TABLE cursoaluno(
	id serial NOT NULL,
	id_aluno integer NOT NULL,
	id_curso integer NOT NULL,
	CONSTRAINT pk_id_cursoaluno PRIMARY KEY (id),
	CONSTRAINT fk_id_cursoaluno FOREIGN KEY (id_aluno) REFERENCES aluno (id),
	CONSTRAINT fk_id_curso FOREIGN KEY (id_curso) REFERENCES curso (id)
);
